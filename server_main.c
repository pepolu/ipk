/*************************************
 * Simple file transfer server
 * usage: ./ipk-server -p [port]
 * 
 * @author Peter Lukáč
 * @author xlukac11
 * 
 * */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>


/**************
 * This just does two things, prints message to stderr and exits with code so i don't have to do it twice in code
 * @param str   String to print
 * @param code  Exit code
 * */
void sysError(char* str, int code);


/**************
 * Main function runs all instrunctions
 * @param argc  Argument count
 * @param argv  Arguments stored in double pointer or what the hell the proper name is
 * */
int main(int argc, char** argv){

    if(argc != 3)
        sysError("missing arguments",1);

    if( strcmp(argv[1],"-p") != 0)
        sysError("invalid argument",1);

    int port = atoi(argv[2]);

    if( port == 0 )
        sysError("port not set",1);
    
    printf("port: %d\n",port);



    int listen_socket;
    struct sockaddr_in server_address;
    int optval = 1;

    // create socket
    if( (listen_socket = socket(AF_INET, SOCK_STREAM, 0)) <= 0 )
        sysError("ERROR in socket creation",1);

    setsockopt(listen_socket, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval , sizeof(int));

    // set address
    bzero((char *) &server_address, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons((unsigned short)port);

    // bind
    if( bind(listen_socket, (struct sockaddr *) &server_address, sizeof(server_address)) < 0)
        sysError("ERROR in socket bind",1);

    // listen
    if((listen(listen_socket, 1)) < 0)
        sysError("audience is NOT listening",1);


    while(1){                                           // server activity begins here
        printf("waiting for connection\n");
        int comm_socket = accept(listen_socket, (struct sockaddr*)NULL, NULL);  // wait for connection and process comunication

        if( comm_socket > 0 ){
            printf("recived valid connection\n");

            int flags = fcntl(comm_socket, F_GETFL, 0);
            if( fcntl(comm_socket, F_SETFL, flags | O_NONBLOCK) < 0 )
                sysError("fcntl() failed",1);

            struct timeval *timePtr = NULL;
            fd_set read_s, write_s;

            FILE* file = NULL;

            int send_data = 0;
            int i = 0;
            int waiting_for_command = 1;
            int recive_data = 0;
            int wait_for_ack = 0;

            while(1){                       // server download/upload part
                FD_ZERO(&read_s);
                FD_ZERO(&write_s);

                FD_SET(comm_socket, &read_s);
                FD_SET(comm_socket, &write_s);

                select( FD_SETSIZE, &read_s, &write_s, (fd_set*)0, timePtr );

                if (FD_ISSET(comm_socket, &read_s)){
                    i = 0;                              // recived data
                    unsigned char buffer[1024];
                    bzero((char*)buffer,1024);
                    int len;
                    len = read(comm_socket, buffer, 1024);
                    if(len == 0){
                        printf("host disconected\n");
                        if( file != NULL )
                            fclose( file );

                        if( send_data == 1 )
                            printf("ERROR host disconected while file download: likely loss of data on client side\n");

                        if( recive_data == 1 )
                            printf("ERROR host disconected while file upload: likely loss of data on server side\n");

                        break;
                    }
                    if(len < 0){
                        printf("error reading from buffer\n");
                        break;
                    }

                    
                    if( len > 2 && waiting_for_command == 1){       // we are waiting for command and we have recived something good
                        if(buffer[0] == 'r' && buffer[1] == ':'){   // reading from server
                            char* name = malloc(len-1);
                            memcpy(name, buffer+2,len-2);
                            name[ len-2 ] = '\0';
                            printf("request to read: %s\n", name);
                            if( (file = fopen(name,"rb")) == NULL){  // if is readable open file
                                char code[1] = {21};                 // if not send NAK
                                write(comm_socket, code, 1);
                                free(name);
                                break;
                            }
                            struct stat isFile;
                            stat(name, &isFile);
                            if( S_ISREG(isFile.st_mode) == 0){  // if is readable open file
                                char code[1] = {21};                 // if not send NAK
                                write(comm_socket, code, 1);
                                free(name);
                                break;
                            }
                            waiting_for_command = 0;
                            send_data = 1;
                            free(name);
                            

                            
                        }else if(buffer[0] == 'w' && buffer[1] == ':'){ // writing data to server
                            char* name = malloc(len-1);
                            memcpy(name, buffer+2,len-2);
                            name[ len-2 ] = '\0';
                            printf("request to write: %s\n", name);
                            if( (file = fopen(name,"wb")) == NULL ){  // if is writeable
                                char code[1] = {21};            // if path wasn't created end
                                write(comm_socket, code, 1);
                                printf("failed to open file to write to\n");
                                free( name );
                                break;
                            }   // if file was easily created go there
                            char code[1] = {6};
                            write(comm_socket, code, 1);
                            free( name );
                            waiting_for_command = 0;
                            recive_data = 1;
                            continue;
                        }
                        else{                          // we didn't find anything
                            printf("unknown request\n");
                        }
                    }
                    if( send_data == 1 && wait_for_ack == 1){   // if sending to client, check for ACK message
                        if( len == 1 && buffer[0] == 6)
                            wait_for_ack = 0;
                    }
                    if( recive_data == 1 ){                     // here we are reciving data
                        if( len == 1 && buffer[0] == 4 ){       // we have recived EOT so we finish
                            fclose(file);
                            file = NULL;
                            printf("recived all data and EOT\n");
                            break;
                        }
                        fwrite(buffer,1,len,file);          
                        char ack[1] = {6};
                        write( comm_socket, ack, 1 );
                    }
                }
                else
                if (FD_ISSET(comm_socket, &write_s) && send_data == 1 && wait_for_ack == 0){    // send chunk of data
                    i = 0;
                    int readLen;
                    unsigned char sendBuff[1024];
                    bzero((char*)sendBuff,1024);
                    if( (readLen = fread(sendBuff, 1, 1000, file)) == 0 ){      // read chunk of data from file
                        char code[1] = {4};                             // if reached end of file send EOT byte
                        write(comm_socket, code, 1);
                        send_data = 0;
                        wait_for_ack = 0;
                        fclose(file);
                        file = NULL;
                    } else{                                         // if data available, send it
                        write(comm_socket, sendBuff, readLen);
                        wait_for_ack = 1;
                    }
                }
            }
        }
        else{
            printf("invalid socket\n");
        }
    }

    return 0;
}


void sysError(char* str, int code){
    fprintf(stderr,"%s\n",str);
    exit(code);
}
