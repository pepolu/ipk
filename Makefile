all: client_main.o server_main.o
	gcc -std=c11 -o ipk-client client_main.o
	gcc -std=c11 -o ipk-server server_main.o

client_main.o : client_main.c
	gcc -std=c11 -c client_main.c

server_main.o : server_main.c
	gcc -std=c11 -c server_main.c

clean:
	rm *.o
