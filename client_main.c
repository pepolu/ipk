/*************************************
 * Simple file transfer client
 * usage: ./ipk-client -p [port] -h [host] -r/-w [file]
 * 
 * @author Peter Lukáč
 * @author xlukac11
 * 
 * */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <netdb.h>


/**************
 * This just does two things, prints message to stderr and exits with code so i don't have to do it twice in code
 * @param str   String to print
 * @param code  Exit code
 * */
void sysError(char* str, int code);


/**************
 * Function creates given path in filesystem
 * @param file  path to be created
 * */
int createPath(char* file);

/**************
 * Function extracts filename from given path
 * @param file  filepath
 * */
char* getFile(char * file);


/**************
 * Main function runs all instrunctions
 * @param argc  Argument count
 * @param argv  Arguments stored in double pointer or what the hell the proper name is
 * */
int main(int argc, char** argv){

    if(argc != 7)
        sysError("missing arguments",1);

    char* host = NULL;
    int port = 0;
    char* fileName = NULL;

    int readingFile;

    for(int i = 1; i < 7; i+=2){
        if( strcmp(argv[i],"-h") == 0 ){
            if( (host = malloc(strlen(argv[i+1]+1))) == NULL )
                sysError("memory allocation error",99);
                
            strcpy(host,argv[i+1]);
            host[ strlen(argv[i+1]) ] = '\0';
            continue;
        }
        if( strcmp(argv[i],"-p") == 0 ){
            port = atoi(argv[i+1]);
            continue;
        }
        if( strcmp(argv[i],"-r") == 0 || strcmp(argv[i],"-w") == 0){
            if( ( fileName = malloc(strlen(argv[i+1]) +1) ) == NULL )
                sysError("memory allocation error",99);
            strcpy(fileName,argv[i+1]);
            fileName[strlen(argv[i+1])] = '\0';
            if( strcmp(argv[i],"-r") == 0 )
                readingFile = 1;
            else
                readingFile = 0;
            continue;
        }
    }

    if( host == NULL )
        sysError("host not set",1);
    
    if( port == 0 )
        sysError("port not set",1);
    
    if( fileName == NULL )
        sysError("file not set",1);
    
    printf("host: %s\n",host);
    printf("path: %s\n", fileName);

    int socket_client;
    if( (socket_client = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
        sysError("ERROR creating socket", 1);

    struct sockaddr_in server_address;
    bzero((char *) &server_address, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons((unsigned short)port);
    if( inet_pton(AF_INET, host, &server_address.sin_addr) <= 0 ){  // create server address, if failes, suspect f.s. DNS name
        struct hostent* resolveHost;
        if( (resolveHost = gethostbyname(host)) == NULL )       // get ip addresses from local cache
            sysError("Couldn't resolve host", 1);

        struct in_addr** new_addr = (struct in_addr**) resolveHost->h_addr_list;
        if( new_addr[0] != NULL  )
            if( inet_pton(AF_INET, inet_ntoa(*new_addr[0]), &server_address.sin_addr) <= 0 ){  // create ip address again, use first address
                sysError("Couldn't fetch ip address of host", 1);
            }
        printf("host resolved to ip address: %s\n", inet_ntoa(*new_addr[0]));
    }

    FILE* file;
    if( readingFile == 0 ){
        file = fopen(fileName,"rb");         // open file to read from
        if( file == NULL ){                  // if can't end before atempt to connect
            sysError("ERROR can't open file", 1);
            free( fileName );
        }
    }

    if( connect(socket_client,              // try to connect, if not, just end
    (struct sockaddr *)&server_address, 
    sizeof(server_address)
    ) !=0 ){
        if( readingFile == 0 )
            fclose(file);

        free( fileName );
        sysError("ERROR failed to connect", 1);
    }

    printf("port: %d\n",port);

    char* nameOnly = getFile(fileName);     // get filename from path+file
    printf("file: %s\n",nameOnly);

    if( readingFile == 1 ){                         // send command to read file from server
        char* sendStr = malloc(strlen(nameOnly)+2);
        bzero((char*)sendStr,strlen(nameOnly)+2);
        sendStr[0] = 'r'; sendStr[1] = ':';
        strcat(sendStr,nameOnly);
        write(socket_client, sendStr, strlen(sendStr));
        free(sendStr);
    }else{                                              // send command to write file to server
        char* sendStr = malloc(strlen(nameOnly)+2);
        bzero((char*)sendStr,strlen(nameOnly)+2);
        sendStr[0] = 'w'; sendStr[1] = ':';
        strcat(sendStr,nameOnly);
        write(socket_client, sendStr, strlen(sendStr));
        free(sendStr);
    }

    free( nameOnly );

    unsigned char buffer[1024];
    int len;
    bzero((char*)buffer,1024);


    if(readingFile == 1){                               // we are reading file from server
        len = recv( socket_client, buffer, 1024, 0 );
        if( len == 1 && buffer[0] == 21){        // file doesn't exist or can't be accessd
            close(socket_client);
            sysError("ERROR no such file (server side)", 1);
        }

        file = fopen(fileName,"wb");         // open file to save
        if( file == NULL ){
            createPath(fileName);
            file = fopen(fileName,"wb");
            if( file == NULL ){
                close(socket_client);
                sysError("ERROR can't open file to write to (client side)", 1);
            }
        }
        free( fileName );

        fwrite(buffer,1,len,file);          // write recived first chunk of data
        char ack[1] = {6};
        write( socket_client, ack, 1 );     // and dend ACK
        while(1){
            bzero((char*)buffer,1024);
            len = recv( socket_client, buffer, 1024, 0 );
            if( len == 1 && buffer[0] == 4){         // we have recived end of transmision
                fclose(file);
                close(socket_client);
                sysError("done reading file\nexited normaly", 0);
            }
            fwrite(buffer,1,len,file);
            write( socket_client, ack, 1 );
        }
    }
    else{                   // we are sending file
        free( fileName );
        len = recv( socket_client, buffer, 1024, 0 );
        if( len == 1 && buffer[0] == 21){        // can't upload file to that destination
            close(socket_client);
            sysError("ERROR can't upload file to that destination", 1);
        }
        else if( len != 1 || buffer[0] != 6 ){       // we didn't recived ACK as expected
            close(socket_client);
            sysError("ERROR unknown comunication1", 1);
        }

        while(1){
            bzero((char*)buffer,1024);
            len = fread(buffer, 1, 1000, file);   // read chunk od data
            if(len == 0){                       // if no data available send EOT and exit
                fclose(file);
                char eot[1] = {4};
                write(socket_client, eot, 1);
                close(socket_client);
                sysError("done uploading\nexited normaly", 0);
            }
            write(socket_client, buffer, len);      // send chunk of data
            bzero((char*)buffer,1024);
            len = recv( socket_client, buffer, 1024, 0 );   // get something and hope it is ACK so we can continue
            if( len == 1 && buffer[0] == 6)
                continue;
            else{
                close(socket_client);
                sysError("ERROR unknown comunication2", 1);
            }
        }

    }

    return 0;
}


void sysError(char* str, int code){
    fprintf(stderr,"%s\n",str);
    exit(code);
}


char* getFile(char* file){
    char* ret;
    for(int i = strlen(file)-1; i >= 0; i--){
            if( file[i] == '/' ){
                    ret = malloc( strlen(file) - i);
                    memcpy(ret, &file[i+1], strlen(file) - (i+1) );
                    ret[ strlen(file) - (i+1) ] = '\0';
                    return ret;
            }
    }
    ret = malloc( strlen(file) + 1);
    memcpy(ret, file, strlen(file) );
    ret[ strlen(file) ] = '\0';
    return ret;
}


int createPath(char* file){
        for( int i = 0 ; i < strlen(file); i++){
                if( file[i] == '/' && i != 0){
                        char* dir = malloc( i+1 );
                        memcpy( dir, file, i );
                        dir[i] = '\0';
                        mkdir(dir, 0755);
                        free(dir);
                }
        }
        return 1;
}
